import 'package:componentes/src/providers/menu_provider.dart';
import 'package:componentes/src/utils/icono_string_util.dart';
import 'package:flutter/material.dart';

class CardPage extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Cards'),
      ),
      body: ListView(
        padding: EdgeInsets.all(10.0),
        children: <Widget>[_cardTipo1(), SizedBox(height: 30.0), _cardTipo2()],
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.adb),
        onPressed: () {
          Navigator.pop(context);
        },
      ),
    );
  }
  Widget _cardTipo1(){
    return Card(
      child: Column(children: <Widget>[
        ListTile(
          title: Text('Soy el Titulo'),
          subtitle: Text(
            'Lorem Ipsum is simply dummy text of the pinting and typesetting industry.'),
          leading: Icon(Icons.photo_album, color: Colors.blue),
        ),
        Row(mainAxisAlignment: MainAxisAlignment.end, children: <Widget>[
          FlatButton(
            child: Text('Cancelar'),
            onPressed: () {},
          ),
           FlatButton(
            child: Text('Ok'),
            onPressed: () {},
          ),
        ],)
      ]),
    );
  }
  /*Widget _cardTipo2(){
    return Card(
      child: Column(children: <Widget>[
        Image(
          image: NetworkImage(
            'https://i.pinimg.com/originals/a1/70/6c/a1706c12720b01f6d0e5fde7d21620eb.jpg'
          ),
        ),
        Container(
          padding: EdgeInsets.all(10.0), child: Text('Este es el texto'),
        )
      ]),
    );
  }*/
   Widget _cardTipo2(){
    return Card(
      child: Column(children: <Widget>[
        FadeInImage(
          image: NetworkImage(
            'https://www.itl.cat/pngfile/big/8-82959_wallpaper-celular-full-hd-incrvel-wallpapers-celular.jpg'
          ),
          placeholder: AssetImage('assets/images/cargando.gif'),
          fadeInDuration: Duration(milliseconds: 200),
          height: 300.0,
          fit: BoxFit.cover,
        ),
        Container(
          padding: EdgeInsets.all(10.0), child: Text('Este es el texto'),
        )
      ]),
    );
  }
}